<?php
namespace F2;

include(__DIR__."/../vendor/autoload.php");

/* not overridden default */
$GLOBALS["f2"]["config"]["defaults"]["some-name"] = function() { return "some-value"; };
asserty('some-value' === config('some-name'));

// Value is overridden, but already cached
$GLOBALS['f2']['config']['some-name'] = function() {
    return 'some-other-value';
};
asserty('some-other-value' === config('some-name'));

