<?php
namespace F2\Config;

/**
 * This class will rarely be used. DOCUMENT_ROOT environment variable 
 */
class WebRootDetector {
    /**
     * Tries to find out what is the public web root. Don't use this function in your code
     */
    public static function findWebRoot() {
        static $webRoot;
        if($webRoot) {
            return $webRoot;
        }
        if (isset($_SERVER['DOCUMENT_ROOT'])) {
            // We'll trust this value
            return $webRoot = $_SERVER['DOCUMENT_ROOT'];
        }
        if (PHP_SAPI === 'cli') {
            // Not running in web server
            throw new Exception("Can't detect web root when running on command line. You could define the DOCUMENT_ROOT environment variable.");
        }
        if (isset($_SERVER['SCRIPT_FILENAME']) && isset($_SERVER['PHP_SELF'])) {
            $split = strrpos($_SERVER['SCRIPT_FILENAME'], $_SERVER['PHP_SELF']);
            if($split !== false) {
                return $webRoot = substr($_SERVER['SCRIPT_FILENAME'], 0, $split);
            }
        }
        if (false !== ($cwd = getcwd())) {
            $dir = dirname($_SERVER['PHP_SELF']);
            $dirL = strlen($dir);
            if(substr($cwd, -$dirL) === $dir) {
                return $webRoot = substr($cwd, 0, -$dirL);
            }
            while(!file_exists($cwd.$_SERVER['PHP_SELF'])) {
                $cwd = dirname($cwd);
            }
            return $webRoot = $cwd;
        }
        throw new Exception("Could not detect web root automatically.");
    }
}
