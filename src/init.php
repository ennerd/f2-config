<?php
namespace F2;

use F2\Config\MissingConfigException;
use F2\Config\Exception;
use function getenv, putenv, file_exists, getcwd, is_dir, is_file, dirname, file_get_contents;

/**
 * F2\env
 *
 * Get a configuration variable from the environment. If the variable is not already set,
 * load dotenv-files from /.env, /default.env then if still not found, infer the value.
 *
 * The env files will not be loaded if the env variable is defined.
 */
function env(string $name):?string {
    static $composerRoot, $cached = [];

    /**
     * Check early (may not be using composer)
     */
    $value = $cached[$name] ?? getenv($name);

    if ($value !== false) {
        return $cached[$name] = $value;
    }

    /**
     * Find the composer root path and do some early maintenance
     */
    if (!$composerRoot) {
        $composerRoot = (function() {
            $checking = getcwd();
            while (!is_dir($checking.'/vendor') || !is_file($checking.'/vendor/autoload.php')) {
                if ($checking !== ($toCheck = dirname($checking))) {
                    $checking = dirname($checking);
                } else {
                    throw new Exception("Unable to find the vendor root path.");
                }
            }
            return $checking;
        })();

        /**
         * Load .env file if it exists
         */
        if (file_exists($path = $composerRoot.'/.env')) {
            foreach (\M1\Env\Parser::parse(file_get_contents($path)) as $key => $val) {
                $cached[$key] = $val;
            }
        }

        /**
         * Load default.env file if it exists
         */
        if (file_exists($path = $composerRoot.'/default.env')) {
            foreach (\M1\Env\Parser::parse(file_get_contents($path)) as $key => $val) {
                if (!isset($cached[$key])) {
                    $cached[$key] = $val;
                }
            }
        }
    }

    /**
     * Resolve env variable
     */
    $resolvers = [
        /**
         * To disable any of the optional paths, define the environment variable as empty in your default.env file. For
         * example:
         *
         * PRIV_ROOT=""  # this will disable all paths that depend on PRIV_ROOT
         */

        // This is normally already configured by the web server
        'DOCUMENT_ROOT' => function () {
            if(PHP_SAPI === 'cli') {
                return null;
            }
            if(isset($_SERVER['DOCUMENT_ROOT'])) {
                return $_SERVER['DOCUMENT_ROOT'];
            }
            try {
                $res = WebRootDetector::detectWebRoot();
                $GLOBALS["f2"]["notices"]["F2\\env"]["DOCUMENT_ROOT"] = "Used F2/Config/WebRootDetector";
                return $res;
            } catch (Exception $e) {
                throw new Exception("The DOCUMENT_ROOT environment variable must be set when not in cli mode.");
            }
        },

        // The path where composer.json resides. May be different from DOCUMENT_ROOT.
        'ROOT' => function() use($composerRoot) {
            return $composerRoot;
        },

        // Path for working with files locally on the server. Should not be served by web server.
        'WORK_ROOT' => function() {
            if($root = env('ROOT')) {
                // Local root must be outside of web root
                if(($dr = env('DOCUMENT_ROOT')) && strpos($dr, $root.'/local')===0) {
                    $GLOBALS["f2"]["notices"]["F2\\env"]["LOCAL_ROOT"] = "Defaulted to $root/local, which is inside DOCUMENT_ROOT";
                    return null;
                }
                $GLOBALS["f2"]["notices"]["F2\\env"]["LOCAL_ROOT"] = "Was set using ROOT";
                return $root.'/local';
            }
            return null;
        },

        // Path for permanently storing files. Files are rarely changed, and must not be accessible via web server. May use S3FS or alternatives.
        'SHARED_ROOT' => function() {
            if($root = env('ROOT')) {
                if(($dr = env('DOCUMENT_ROOT')) && strpos($dr, $root.'/shared')===0) {
                    $GLOBALS["f2"]["notices"]["F2\\env"]["SHARED_ROOT"] = "Defaulted to $root/shared, which is inside DOCUMENT_ROOT";
                    return null;
                }
                $GLOBALS["f2"]["notices"]["F2\\env"]["SHARED_ROOT"] = "Was set using ROOT";
                return $root.'/shared';
            }
            return null;
        },

        // Path for permanently storing files. Files are rarely changed, and MUST be accessible via web server. May use S3FS or alternatives.
        // Must match PUBLIC_URL
        'PUBLIC_ROOT' => function () {
            if($root = env('DOCUMENT_ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["PUBLIC_ROOT"] = "Defaulted to $root/files";
                return $root.'/files';
            }
            return null;
        },

        // Web application source code and files. Ideally placed outside of DOCUMENT_ROOT, but not a requirement.
        'APP_ROOT' => function () {
            if($root = env('ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["APP_ROOT"] = "Defaulted to $root/files";
                return $root.'/app';
            }
            return null;
        },

        // Path to configuration files. Should NOT be accessible via web server.
        'CONFIG_ROOT' => function () {
            if($root = env('APP_ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["CONFIG_ROOT"] = "Defaulted to $root/config";
                return $root.'/config';
            }
            return null;
        },

        'VIEWS_ROOT' => function () {
            if($root = env('APP_ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["VIEWS_ROOT"] = "Defaulted to $root/views";
                return $root.'/views';
            }
            return null;
        },

        'SEEDS_ROOT' => function () {
            if($root = env('APP_ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["SEEDS_ROOT"] = "Defaulted to $root/seeds";
                return $root.'/seeds';
            }
            return null;
        },

        'MIGRATIONS_ROOT' => function () {
            if($root = env('APP_ROOT')) {
                $GLOBALS["f2"]["notices"]["F2\\env"]["MIGRATIONS_ROOT"] = "Defaulted to $root/migrations";
                return $root.'/migrations';
            }
            return null;
        },

        'TEMP_ROOT' => function() {
            if($root = env('WORK_ROOT')) {
                if(($dr = env('DOCUMENT_ROOT')) && strpos($dr, $root.'/temp')===0) {
                    return null;
                }
                $GLOBALS["f2"]["notices"]["F2\\env"]["TEMP_ROOT"] = "Defaulted to $root/temp";
                return $root.'/temp';
            }
            if ($root = sys_get_temp_dir()) {
                $candidate = $root."/".str_replace(["/", DIRECTORY_SEPARATOR], "_", trim(env("ROOT"), DIRECTORY_SEPARATOR."/"));
                if (!is_dir($candidate)) {
                    $m = umask(0);
                    if (!mkdir($candidate, 01770)) {
                        umask($m);
                        throw new Exception("TEMP_ROOT environment variable not set. Tried to create $candidate, but failed.");
                    }
                    umask($m);
                }
                $GLOBALS["f2"]["notices"]["F2\\env"]["TEMP_ROOT"] = "Defaulted to $candidate";
                return $candidate;
            }
            return null;
        },

        // REQUIRED for web applications
        'BASE_URL' => function () {
            if (PHP_SAPI === 'cli') {
                return null;
            }
            $baseUrl = (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http') . $_SERVER['HTTP_HOST'];
            $GLOBALS["f2"]["notices"]["F2\\env"]["BASE_URL"] = "Defaulted to $baseUrl";
            return $baseUrl;
        },

        // Web path for files in PUBLIC_ROOT
        'PUBLIC_URL' => function () {
            if (PHP_SAPI === 'cli') {
                return null;
            }
            if ($bu = env("BASE_URL")) {
                // If PUBLIC_ROOT is inside DOCUMENT_ROOT we make some assumptions
                $pr = env("PUBLIC_ROOT");
                $dr = env("DOCUMENT_ROOT");
                if ($pr && $dr) {
                    $pr = realpath($pr);
                    $dr = realpath($dr);
                    if (!$pr || !$dr) {
                        $GLOBALS["f2"]["notices"]["F2\\env"]["PUBLIC_URL"] = "Both PUBLIC_ROOT and DOCUMENT_ROOT must exist";
                        return null;
                    }
                    $prl = strlen($pr);
                    $drl = strlen($dr);
                    if (substr($pr, 0, $drl) === $dr) {
                        $bu .= substr($pr, $drl);
                        $GLOBALS["f2"]["notices"]["F2\\env"]["PUBLIC_URL"] = "Defaulted to $bu";
                        return $bu;
                    }
                }
                $GLOBALS["f2"]["notices"]["F2\\env"]["PUBLIC_URL"] = "Can't infer value without BASE_URL";
                return null;
            }
            return (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http') . $_SERVER['HTTP_HOST'] . "/files";
        },

        // Doctrine style database configuration
        'DATABASE_URL' => function () {

            // Try to detect database url using common environment variables
            // TODO. See docksal.io, OPENSHIFT_MYSQL_DB_HOST, https://docs.craftcms.com/v3/config/db-settings.html

            // Fallback.
            if ($root = env("DOCUMENT_ROOT") && $host = env("HTTP_HOST")) {
                $root = dirname($root);
                if (!@file_put_contents($tmp = $root . "/.is-writable", "yes")) {
                    return null;
                }
                unlink($tmp);
                $GLOBALS["f2"]["notices"]["F2\\env"]["DATABASE_URL"] = "Using dev database file in $root";
                return 'sqlite:///' . $root . '/DEV-DATABASE-' . $host . '.sqlite';
            }

            return 'sqlite:///'.$dbRoot.'/database.sqlite3';
        }
    ];

    if (isset($resolvers[$name])) {
        return $cached[$name] = call_user_func($resolvers[$name]);
    } else {
        return null;
    }
}

/**
 * F2/config
 *
 * Configuration can be defined several places for F2 components:
 *
 * Lazy configuration. For example loading the config from a file:
 *     $GLOBALS["f2"]["config"][$name] = function() { return "config" };
 *
 * Eager configuration:
 *     $GLOBALS["f2"]["config"][$name] = "config"
 *
 * Load config from the CONFIG_ROOT configuration or environment variable:
 *     $GLOBALS["f2"]["config"][$name] = include(env("CONFIG_ROOT")."/".$name.".php");
 *
 * Components are encouraged to provide "smart" functionality to automatically
 * detect configuration settings:
 *     $GLOBALS["f2"]["config"]["defaults"][$name] = function() { return "config"; }
 *
 * @param $configFile is a path name with no extension, using / as separator
 */
function config(string $configFile) {
    if (!isset($GLOBALS["f2"]["config"])) {
        $GLOBALS["f2"]["config"] = [];
    }

    $config = &$GLOBALS["f2"]["config"];

    if (isset($config[$configFile])) {
        if (is_callable($config[$configFile])) {
            return $config[$configFile] = call_user_func($config[$configFile]);
        } else {
            return $config[$configFile];
        }
    }

    $configRoot = $config["CONFIG_ROOT"] ?? env("CONFIG_ROOT") ?? null;

    if ($configRoot === null) {
        throw new Exception("Path to configuration not set in environment variable CONFIG_ROOT.");
    } else {
        $path = $configRoot . "/" . $configFile . ".php";
        if (file_exists($path)) {
            return $config[$configFile] = include($path);
        }
    }

    if (isset($config["defaults"][$configFile])) {
        if (!is_callable($config["defaults"][$configFile])) {
            throw new Exception("Default config must be configured by a callback");
        }
        return $config[$configFile] = call_user_func($config["defaults"][$configFile]);
    }

    throw new Config\MissingConfigException($configFile);
}
