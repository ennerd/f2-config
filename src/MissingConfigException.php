<?php
namespace F2\Config;

class MissingConfigException extends Exception {

    public function __construct(string $config) {
        parent::__construct("The config file '$config' is missing.");
    }

}
